import React from 'react'
import _JSXStyle from 'styled-jsx/style';
import ReactDOM from 'react-dom/server'
import flush from 'styled-jsx/server'


class PanelWithParam extends React.Component {
    render() {
        return (
            <div>
                <h2>Title</h2>
                <p>{this.props.children}</p>
                <style jsx>{`
                    p {
                        padding: ${ 'large' in this.props ? '50' : '20' }px;
                        background: ${this.props.background};
                    }
                `}</style>
                <style jsx>{`
                    p {
                        color: #999;
                        display: inline-block;
                        font-size: 1em;
                    }
                `}</style>
            </div>
        )
    }
}

class PanelWithoutParam extends React.Component {
    render() {
        return (
            <div>
                <h2>Title PanelWithoutParam</h2>
                <p>{this.props.children}</p>
                <style jsx>{`
                    p {
                        color: #999;
                        display: inline-block;
                        font-size: 1em;
                    }
                `}</style>
            </div>
        )
    }
}

class PanelMultiJSXStyle extends React.Component {
    render() {
        return (
            <div>
                <h2>Title</h2>
                <p>{this.props.children}</p>
                <footer>
                    {/* <style jsx>{`
                        p {
                            color: #FFF;
                        }
                    `}</style> */}
                </footer>
                <style jsx>{`
                    p {
                        color: #999;
                        display: inline-block;
                        font-size: 1em;
                    }
                `}</style>
                <style jsx>{`
                    p {
                        padding: ${ 'large' in this.props ? '50' : '20' }px;
                    }
                `}</style>
            </div>
        )
    }
}

class PanelViaClassName extends React.Component {
    render() {
        return (
            <div>
                <h2>Title</h2>
                <p className={'large' in this.props && 'large'}>{this.props.children}</p>
                <style jsx>{`
                    p {
                        color: #999;
                        display: inline-block;
                        font-size: 1em;
                    }
                    .large {
                        font-size: 18px;
                    }
                `}</style>
            </div>
        )
    }
}

// Demo 唯一ClassName影响范围
class Index extends React.Component {
       render() {
        return (
            <div>
                <div className="wrapper">
                    <h1>wrapper 1</h1>
                </div>
                <div className="wrapper">
                    <h1>wrapper 2</h1>
                    <p>Done is Done</p>
                    <div>
                        <p>Do it</p>
                        <ul>
                            <li>Name</li>
                            <li>Age</li>
                        </ul>
                    </div>
                    <style jsx>{`
                        .wrapper {                            
                            border: 1px solid #ccc;
                        }
                        p {
                            color: blue;
                        }
                        h1 {
                            color: #FFF;
                        }
                        div {
                            background: red;
                        }
                        @media (max-width: 600px) {
                            div {
                            background: blue;
                            }
                        }
                        .large {
                            font-size: 1.25em;
                        }
                    `}</style>
                </div>
            </div>   
        )
    }
}

// 参数化
class Demo1 extends React.Component {
    render() {
     return (
         <div>             
             <PanelWithParam background="#CCC">PanelWithParam 1</PanelWithParam>
             <PanelWithParam large>PanelWithParam 2</PanelWithParam>

             {/* <PanelWithoutParam>PanelWithoutParam 1</PanelWithoutParam>
             <PanelWithoutParam>PanelWithoutParam 2</PanelWithoutParam>
             
             <PanelMultiJSXStyle>PanelMultiJSXStyle</PanelMultiJSXStyle>

             <PanelViaClassName>PanelViaClassName</PanelViaClassName>
             <PanelViaClassName large>PanelViaClassName</PanelViaClassName> */}
         </div>   
     )
 }
}

// 组件切换
class Demo2 extends React.Component {
    state = {
        tab: 1
    }
    toggle = () => {
        this.setState((state) => {
            return {
                tab: (state.tab + 1) % 2
            }
        })
    }
    render(){
        var child = this.state.tab === 1 ? (
            <div>
                <h2>Tag1</h2>
                <p>bula bula</p>
                <style jsx>{`
                    h2 {
                        color: red;
                    }
                `}</style>
            </div>
        ) : (
        <div>
            <h2>Tag2</h2>
            <p>bula bula</p>
            <style jsx>{`
                h2 {
                    color: blue;
                }
                p {
                    font-size: 1.5em;
                    font-weight: 600;
                }
            `}</style>
            <style jsx global>{`
                body {
                    background: #ccc;
                }
            `}</style>
        </div>);
        return (
            <div>
                <button onClick={this.toggle}>Toggle</button>
                {child}
            </div>
        )
    }
}

// 组件切换 _JSXStyle
class Demo3 extends React.Component {
    state = {
        tab: 1
    }
    toggle = () => {
        this.setState((state) => {
            return {
                tab: (state.tab + 1) % 2
            }
        })
    }
    render(){
        var child = this.state.tab === 1 ? (
            <div>
                <h2>Tag1</h2>
                <p>bula bula</p>
                <_JSXStyle id="1">{`
                    h2 {
                        color: red;
                    }
                `}</_JSXStyle>
            </div>
        ) : (
        <div>
            <h2>Tag2</h2>
            <p>bula bula</p>
            <_JSXStyle id="2">{`
                h2 {
                    color: blue;
                }
                p {
                    font-size: 1.5em;
                    font-weight: 600;
                }
            `}</_JSXStyle>
        </div>);
        return (
            <div>
                <button onClick={this.toggle}>Toggle</button>
                {child}
            </div>
        )
    }
}

// import styles from '../src/style/demo4.css'
// 外部文件：Styles in regular CSS files
// class Demo4 extends React.Component {
//     render() {
//      return (
//          <div>             
//              <h1>Hello styled-jsx</h1>
//              <style jsx>{styles}</style>
//          </div>   
//      )
//  }
// }

export default Demo3;


/**
 * 标签：full（功能全）,scoped（作用域）, component-friendly(组件友好的)，服务端客户端都可以用
 * 1. 生成唯一的className
 * 2. _JSXStyle
 * 第一步：
 * - 生成唯一的key（一个数字），用于生产唯一的ClassName和ID
 * - 把<style jsx>组件转成_JSXStyle组件，
 * - 给受影响的元素增加唯一 ClassName
 *  父元素，兄弟元素，子孙元素(为啥采用这种策略？)
 *  不影响组件？
 * 
 *  如果元素受多个__JSXStyle影响怎么处理呢？
 * 
 * 3. 最终的产物
 * 每个_JSXStyle组件都会产生一个<style>元素插入head里，多个_JSXStyle产生的style元素的顺序跟_JSXStyle顺序一致。
 * 
 * 4. 作用域class
 *  实现方案
 * - 唯一的className修饰
 * - 多次使用作用域Class
 *      1) 如果包含参数化生成CSS，则会生成不同的className
 *         但这样可能会造成部分冗余，所以建议把静态的style和动态的style分开写在不同的<style jsx>里
 *      2）否则唯一的className一样。
 * 
 * 同一个组件内<style jsx>标签不准许嵌套，即同一组件内的所有<style jsx>标签必须是平级的。
 * 
 *  SyntaxError: D:\workMore\hello-nextjs\pages\index.js: Detected nested style tag: 
 *  styled-jsx only allows style tags to be direct descendants (children) of the outermost JSX element i.e. the subtree root.
 * 
 * 
 * 5. 全局class 
 * de-duping system
 * 
 * 6. <style jsx>卸载时生成的style也会被删除的
 * 
 * Issues：
 * 1. 没法IDE联想？【Done】
 * 2. Via className toggling 这个跟styled-jsx没关系吧
 * 3. 这是什么写法
 * import { colors, spacing } from '../theme'
 * import { invertColor } from '../theme/utils'
 */